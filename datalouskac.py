#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import sys
import re
import datetime
from itertools import imap, izip
from collections import OrderedDict

# universal line parser  Y       M       D       h       m       s    rest
row_re = re.compile(r'(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s(.*)')
delimiter_re = re.compile(r'[\t;,]+')  # trust that delimiters are only \t , ;
data_re = re.compile(r'(\{[\s\w,\.\[\]\-\(\)]*\})|([\w\[\],\.\-\(\)]+)|([\d\.,\-]+)')

functions = {"half_merge", }
default_function = "half_merge"

def main():
    if len(sys.argv) < 3 or "-h" in sys.argv or "--help" in sys.argv:
        print "Usage: ./datalouskac.py [function-name] <file file ...>"
        print "Available functions: {0}".format(", ".join(f for f in functions))
        print "Will produce file result.csv"
        return

    # parse function name and files from args
    if sys.argv[1] in functions:
        function_name = sys.argv[1]
        files = sys.argv[2:]
    else:
        function_name = default_function
        files = sys.argv[1:]
    try:
        function = globals()[function_name]
    except KeyError:
        print "Function you are calling does not exist"

    return function(files)


def half_merge(files):
    datasources = map(DataSource, files)
    cummulate = DataSource('result.csv')
    for ds in datasources:
        ds.parse()
        ds.cummulate()
        cummulate.merge(ds)
    cummulate.write()


class DataSource(object):

    def __init__(self, filename):
        self.filename = filename
        self.rows = OrderedDict()

    def parse(self):
        if self.rows:
            return

        with open(self.filename, "r") as fp:
            for line in fp:
                try:
                    self._add_row(line)
                except ValueError as e:
                    print "Skipping line \"{0}\"".format(line)
                    print e

    def _add_row(self, line):
        mo = row_re.match(line)
        if not mo:
            raise ValueError("Line \"{0}\" has a weird format!".format(line))
        timestamp = datetime.datetime(*map(int, mo.group(1, 2, 3, 4, 5, 6)))  # YMDhms
        raw_data = data_re.findall(mo.group(7).strip())  # the rest
        if not raw_data:
            raise ValueError("Data \"{0}\" has a weird format!".format(mo.group(7).strip()))
        # process data types in rest int, float, NaN
        data = []
        for x in raw_data:
            if isinstance(x, (tuple, list)):
                for xx in x:
                    if xx:
                        x = xx
                        break;
                else:
                    continue  # the tuple is empty
            try:
                data.append(int(x))
                continue
            except ValueError: pass

            try:
                data.append(float(x))
                continue
            except ValueError: pass

            if x.strip(" ").startswith("{"):
                x = x.replace("\t", " ")
            data.append(x)

        self.rows[timestamp] = data

    def _round_dt(self, timestamp):
        mins = timestamp.minute
        rounded = datetime.datetime(timestamp.year, timestamp.month,
            timestamp.day, timestamp.hour, timestamp.minute, 0)  # second = 0
        if mins < 30:
            return rounded - datetime.timedelta(minutes=mins)
        return rounded + datetime.timedelta(minutes=(60 - mins))

    def _merge_rows(self, a, b):
        merged = []
        for x, y in izip(a, b):
            if isinstance(x, (int, float)) and isinstance(y, (int, float)):
                merged.append(x+y)
                continue

            if isinstance(x, (int, float)):
                merged.append(x)
                print "Merging {0} and {1} - choosing numerical value".format(x, y)
                continue

            if isinstance(y, (int, float)):
                merged.append(y)
                print "Merging {0} and {1} - choosing numerical value".format(x, y)
                continue

            if type(x) == type(y) and x == y:
                merged.append(x)
                continue

            if isinstance(x, (str, unicode)) and isinstance(y, (str, unicode)):
                if x.strip().startswith("{") and y.strip().startswith("{"):
                    # if string and contains '{' - join the sets
                    tmp = " ".join((x.strip(" {}"), y.strip(" {}")))
                    merged.append("{" + tmp + "}")
                    continue
            choice = input("Merging A={0} and B={1}\n"
                           "1) use A\n"
                           "2) use B\n"
                           "3) use '+' operator\n"
                           "4) type result by yourself\n"
                           "Choice : ".format(x, y))
            if choice == 1:
                merged.append(x)
                continue
            if choice == 2:
                merged.append(y)
                continue
            if choice == 3:
                merged.append(x+y)
                continue
            result = input("Result (surround string with \"): ")
            merged.append(result)
        return merged

    def cummulate(self):
        '''Cummulate rows for every hour (from h:30 to h+1:30).
        It changes the timestamp to middle value - h+1:00'''
        cummulated = OrderedDict()
        for ts, data in self.rows.iteritems():
            rounded = self._round_dt(ts)
            if rounded in cummulated:
                cummulated[rounded] = self._merge_rows(cummulated[rounded], data)
            else:
                cummulated[rounded] = data
        self.rows = cummulated

    def merge(self, other):
        '''Merges data from other to own based on the same timestamp'''
        new_rows = OrderedDict()

        if not self.rows:
            self.rows = other.rows
            return

        for k, v in other.rows.iteritems():
            if k in self.rows:
                # only if the key exists in both then merge
                new_rows[k] = self.rows[k] + v
        self.rows = new_rows

    def write(self):
        with open(self.filename, "w") as fp:
            fp.writelines(imap(self.as_csv, self.rows.iteritems()))

    def as_csv(self, row):
        timestamp = "\t".join(map(str, row[0].timetuple()[:6]))
        data = "\t".join(map(str, row[1]))
        return "\t".join((timestamp, data)) + '\n'

if __name__ == '__main__':
    main()

