#!/usr/bin/python
#coding:utf-8

import datetime
import sys
import io
import re
import os


class AnalyzeObject:

    pattern = None

    def time(self, line):
        if line == '':
            return None
        matchObj = self.pattern.match(line.strip())
        if matchObj is None:
            return None
        time = list(matchObj.groups()[0:4])
        time = map(int, time) + [0, 0]
        return datetime.datetime(*time)

    def data(self, line):
        if line is None or line == '':
            return ["", ]
        matchObj = self.pattern.match(line)
        return list(matchObj.groups()[6:])


class Ray(AnalyzeObject):

    pattern = re.compile(
        r"(\w)[\s;](?P<years>\d*)[\s;](?P<months>\d*)[\s;](?P<days>\d*)[\s;]" +
        r"(?P<hours>\d*)[\s;](?P<minutes>\d*)[\s;](?P<seconds>\d*)[\s;]" +
        r"(?P<microseconds>[0-9]*)\.?[0-9]*[\s;](.*)"
        )

    def data(self, line):
        if line is None or line == '':
            return []
        matchObj = self.pattern.match(line)
        return matchObj.group(8).replace(",", ".").split(" ")

    def time(self, line):
        if line == '':
            return None
        matchObj = self.pattern.match(line)
        if matchObj is None:
            return None
        return datetime.datetime(
            int(matchObj.group("years")),
            int(matchObj.group("months")),
            int(matchObj.group("days")),
            int(matchObj.group("hours")),
            0, 0, 0  # we do not want minutes
            )


class Pressure(AnalyzeObject):
    pattern = re.compile(
        r'(?P<years>\d*)\s(?P<months>\d*)\s(?P<days>\d*)\s'
        r'(?P<hours>\d*)\s(?P<minutes>\d*)\s(?P<seconds>\d*)'
        r'\s(.*)'
        )

    def data(self, line):
        if line is None or line == '':
            return []
        matchObj = self.pattern.match(line)
        return matchObj.group(7).replace(",", ".").split(" ")


def merge(ostream, rline, pline):
    time = ray.time(rline) or pressure.time(pline)
    if not ray.data(rline) or not pressure.data(pline):
        return

    data = ray.data(rline) + pressure.data(pline)
    time = time.timetuple()[:6]
    ostream.write(";".join(map(str, time)))
    ostream.write(";")
    ostream.write(";".join(data))
    ostream.write(";\n")


def run():
    if len(sys.argv) != 4:
        print "Usage: ./merge.py ray_file pressure_file outfile"
        sys.exit()

    try:
        rays = io.BufferedReader(io.FileIO(sys.argv[1], "r"))
        pressures = io.BufferedReader(io.FileIO(sys.argv[2], "r"))
    except IOError:
        print "One of the files does not exist"
        try:
            rays.close()
            pressures.close()
        except:
            pass
        sys.exit()

    try:
        out = io.BufferedWriter(io.FileIO(sys.argv[3], "w"))
    except IOError:
        path = os.path.dirname(sys.argv[3])
        if not os.path.isdir(path):
            os.mkdir(path)

    try:
        out = io.BufferedWriter(io.FileIO(sys.argv[3], "w"))
    except:
        rays.close()
        pressures.close()
        raise

    rline = rays.readline()
    pline = pressures.readline()

    while True:
        rline = rline.strip()
        pline = pline.strip()
        if rline == '' or pline == '':
            break  # we do not want empty columns

        #~ if rline == '':
            #~ merge(out, rline, pline)
            #~ pline = pressures.readline()
            #~ continue
#~
        #~ if pline == '':
            #~ merge(out, rline, pline)
            #~ rline = rays.readline()
            #~ continue

        rtime = ray.time(rline)
        ptime = pressure.time(pline)

        if rtime is None:
            rline = rays.readline()
            continue

        if ptime is None:
            pline = pressures.readline()
            continue

        if rtime == ptime:
            merge(out, rline, pline)
            pline = pressures.readline()
            rline = rays.readline()
            continue

        if rtime < ptime:
            #~ merge(out, rline, '')  # do not print empty data sets
            rline = rays.readline()
        else:
            #~ merge(out, '', pline)  # do not print empty data sets
            pline = pressures.readline()

    rays.close()
    pressures.close()
    out.close()


if __name__ == "__main__":
    ray = Ray()
    pressure = Pressure()

    run()
