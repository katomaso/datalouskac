#!/usr/bin/python
#coding:utf-8

from Tkinter import *
from ttk import *
from exceptions import Exception

import Tkconstants, tkFileDialog

import os
from os.path import join
import analyzers


class DataLouskacGUI(Frame):
    # filenames to operate on
    filenames = []

    # label where show notifications
    l_notice = None

    # open buttons
    b_openfiles = None
    b_opendir = None

    analyzers = analyzers.analyzers

    _buttons = []

    def __init__(self, root):

        Frame.__init__(self, root)
        # options for buttons
        button_opt = {'padx': 2, 'pady': 2, 'ipadx': 5, 'ipady': 5}

        self.b_openfiles = Button(self, text=u"Vyber soubor",
        command=self.openfiles).pack(fill=X, **button_opt)
        self.b_opendir = Button(self, text=u"Vyber složku",
        command=self.opendir).pack(fill=X, **button_opt)

        Label(self, text=u"Dostupné analýzy").pack(fill=X, **button_opt)

        # define buttons
        row = 2
        for title,fn in self.analyzers:
            fn = getattr(analyzers, fn)
            self._buttons.append(Button(self, text=title,
            command=lambda: self.analyze(fn)).pack(fill=X, **button_opt) )
            row += 1

        self.l_notice = Listbox(root)
        self.l_notice.pack(side=BOTTOM, fill=BOTH, expand=1, **button_opt)

        # define options for opening or saving a file
        self.file_opt = options = {}
        options['filetypes'] = [('text files', '.txt'),
                                        ('csv files', '.csv'),
                                        ('all files', '.*')]
        options['initialdir'] = join(os.getcwd(), "..", "data")
        options['title'] = 'Select file to analyze'

        # defining options for opening a directory
        self.dir_opt = options = {}
        options['initialdir'] = os.path.abspath(os.path.dirname("."))
        options['mustexist'] = True
        options['title'] = 'Analyzator souboru'

        self.notify(u"Datalouskač připraven")

    def notify(self, msg):
        self.l_notice.insert(END, msg)

    def openfiles(self):
        self.filenames = tkFileDialog.askopenfilename(**self.file_opt)
        if isinstance(self.filenames, (str, unicode)):
            self.filenames = [self.filenames, ]

        for f in self.filenames:
            self.notify(u"+ %s" % f)

    def opendir(self):
        datadir = tkFileDialog.askdirectory(**self.dir_opt)
        self.filenames = []

        for fn in os.listdir(datadir):
            if os.path.splitext(fn)[1] in (".txt", ".csv"):
                self.filenames.append(os.path.join(datadir, fn))
                self.notify(u"+ %s" % fn)
            else:
                self.notify(u"- %s" % fn)

    def analyze(self, func, *args, **kwargs):
        originaldir = os.path.dirname(self.filenames[0])
        ot = originaldir.rsplit(str(os.sep), 1)[0]
        newdir = os.path.join(ot, func.__name__)

        if not os.path.isdir(newdir):
            os.mkdir(newdir)

        for fn in self.filenames:
            outfn = os.path.join(newdir, os.path.split(fn)[1])
            outfn = os.path.splitext(outfn)[0] + ".csv"
            self.notify("Volam %s, vysledek %s" % (func.__name__, outfn))
        try:
            func(fn, outfn)
        except Exception as e:
            self.notify("Exception: %s" % e)
            raise
        self.notify("Hotovo")

if __name__ == '__main__':
    root = Tk()
    root.minsize(460, 600)
    root.title(u"Datalouskač")
    DataLouskacGUI(root).pack()
    root.mainloop()
