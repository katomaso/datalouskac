# coding:utf-8
'''
@file Analyzers.py
Library with functions with prototype: 
def fn_name(input_filename, output_filename)

These functions are called via DataLouskacGUI if added into analyzers
'''

from datetime import *
import re
import io
import os
from os.path import join
import logging

'''
Add here you analyzer method to available in GUI be performed
'''
# BUG: funguje jen analyzator, ktery je posledni v seznamu.
analyzers = (
# ("Nazev tlacitka", 'nazev_funkce' )
(u"Ukazkovy analyzator", 'example'),
(u"Soucty po hodine a 5 minutach", 'timesum'),
(u"Soucty po hodine okolo XX:30", 'timesum_half'),
)
  
def example(infile, outfile):
  '''Example analyzer which just takes input filename and output filename'''
  fi = open(infile, "r")
  fo = open(outfile, "w")
  
  for line in fi:
    reversed_numbers = line.split(" ")
    reversed_numbers.reverse()
    outline = ";".join(reversed_numbers)
    fo.write(outline + "\n")
  
  fi.close()
  fo.close()


def only_a(fi, fo):
  fi = open(infile, "r")
  fo = open(outfile, "w")
  
  for line in fi:
    if line.startswith("a"):
      fo.write(outline + "\n")
  
  fi.close()
  fo.close()


def timesum_half(infile, outfile):
  fi = open(infile, "r")
  fo = open(outfile, "w")
  
  # first, go to the half an hour
  origin = get_first_datetime(fi)
  
  if origin.minute < 30:
    origin = datetime(origin.year, origin.month, origin.day, origin.hour, 30, 0) - timedelta(hours=1)
  else:
    origin = datetime(origin.year, origin.month, origin.day, origin.hour, 30, 0)
    
  pattern = re.compile("(\w) (?P<years>\d*) (?P<months>\d*) (?P<days>\d*) "+
                       "(?P<hours>\d*) (?P<minutes>\d*) (?P<seconds>\d*) "+
                       "(?P<microseconds>[0-9]*)\.[0-9]* "+
                       "(\d*) (\d*) (\d*) (\d*) (\d*) (\d*) "+
                       "([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*).*"
                       )
  cummulated_data = [ 0 for x in range(11) ]
  over_full = False
  line_num = 0
  threshold = timedelta(hours=1)
  
  for line in fi:
    mm = re.match(pattern, line)
    if mm is None: continue
    
    # preskocit v pripade jineho symbolu nez 'a'
    if mm.group(1) != 'a':
      continue
      
    cummulated_data[10] += 1
    line_num += 1
    
    minutes = int(mm.group("minutes"))
    
    actual = datetime(
        int(mm.group("years")),
        int(mm.group("months")),
        int(mm.group("days")),
        int(mm.group("hours")),
        int(mm.group("minutes")),
        int(mm.group("seconds")),
        int(mm.group("microseconds")) % 999999
        )
    delta = actual-origin
    
    if minutes < 30:
      over_full = True
      
    # end when we are over xx:30
    if delta > threshold:
      if origin.minute < 30:
        origin = datetime(actual.year, actual.month, actual.day, actual.hour, 30, 0) - timedelta(hours=1)
      else:
        origin = datetime(actual.year, actual.month, actual.day, actual.hour, 30, 0)
      
      # write data and let gen_sums do the rest
      timedata = [
        int(mm.group("years")),
        int(mm.group("months")),
        int(mm.group("days")),
        int(mm.group("hours")),
        "00", "00", "0.0"]
      write_data(fo, timedata, cummulated_data)
      
      # get ready for next run
      cummulated_data = [ 0 for x in range(11) ]
      
      # TODO: zjistit tohle
      #cummulated_data[10] = 1
    
    else:
      # jsme pod limitem .. proste pricteme data
      for i in range(10):
        cummulated_data[i] += float(mm.group(i+9))
  #endfor
  
  if cummulated_data[0] != 0 and mm is not None:
    # write data and let gen_sums do the rest
    timedata = [
      int(mm.group("years")),
      int(mm.group("months")),
      int(mm.group("days")),
      int(mm.group("hours")),
      "00", "00", "0.0"]
    write_data(fo, timedata, cummulated_data)
          
  fi.close()
  fo.close()

# do the summing by the time
def timesum(ifn, ofn):
  base, ext = os.path.splitext(ofn)
  
  with open(ifn, "r") as istream:
    with open("%s.%s%s" % (base, "5min", ext), "w") as ostream:
      gen_sums(istream, ostream, timedelta(minutes=5))
    istream.seek(0)
    
    with open("%s.%s%s" % (base, "1hod", ext), "w") as ostream:
      gen_sums(istream, ostream, timedelta(hours=1))


def get_first_datetime(instream):
  pattern = re.compile("(\w) (?P<years>\d*) (?P<months>\d*) (?P<days>\d*) "+
                       "(?P<hours>\d*) (?P<minutes>\d*) (?P<seconds>\d*) "+
                       "(?P<microseconds>[0-9]*)\.[0-9]* "+
                       "(\d*) (\d*) (\d*) (\d*) (\d*) (\d*) "+
                       "([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*).*"
                       )
  for line in instream:
    
    mm = re.match(pattern, line)
    if mm is None: continue
    
    # preskocit v pripade jineho symbolu nez 'a'
    if mm.group(1) != 'a':
      continue
    
    instream.seek(0)
    return datetime(
        int(mm.group("years")),
        int(mm.group("months")),
        int(mm.group("days")),
        int(mm.group("hours")),
        int(mm.group("minutes")),
        int(mm.group("seconds")),
        int(mm.group("microseconds")) % 999999
        )
          
  raise Exception("No datetime found")


def gen_sums(instream, ostream, treshold):
  origin = datetime(year=2000, month=1, day=1)
  pattern = re.compile("(\w) (?P<years>\d*) (?P<months>\d*) (?P<days>\d*) "+
                       "(?P<hours>\d*) (?P<minutes>\d*) (?P<seconds>\d*) "+
                       "(?P<microseconds>[0-9]*)\.[0-9]* "+
                       "(\d*) (\d*) (\d*) (\d*) (\d*) (\d*) "+
                       "([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*).*"
                       )
  cummulated_data = [ 0 for x in range(11) ]
  first = True

  for line in instream:
    mm = re.match(pattern, line)
    if mm is None: continue
    
    # preskocit v pripade jineho symbolu nez 'a'
    if mm.group(1) != 'a':
      continue
    cummulated_data[10] += 1
    
    actual = datetime(
        int(mm.group("years")),
        int(mm.group("months")),
        int(mm.group("days")),
        int(mm.group("hours")),
        int(mm.group("minutes")),
        int(mm.group("seconds")),
        int(mm.group("microseconds")) % 999999
        )
    delta = actual-origin
    if delta < treshold:
      # jsme pod limitem .. proste pricteme data
      for i in range(10):
        cummulated_data[i] += float(mm.group(i+9))
       
    else:
      # prekrocen casovy limit ... hura zapsat do souboru
      if not first:
        write_data(ostream, mm.groups()[1:8], cummulated_data)
      
      # get ready for next run
      cummulated_data = [ 0 for x in range(11) ]
      cummulated_data[10] += int(first)
      origin = actual
      first = False
  
  
def write_data(ostream, dt, data):
  ostream.write("a")
  for d in dt: ostream.write(";"+str(d))
  for d in data: ostream.write(";"+str(d))
  ostream.write("\n")
                
