#!/usr/bin/python
#coding:utf-8

'''
This script merges rays with pressure so that it assign pressure to
each rays' record which is between n:30 and n+1:30 where n is time
of messurement of pressure.
'''

import datetime
import sys
import io
import re
import os


class AnalyzeObject:

    pattern = None
    dataStart = 0
    dataCount = 0

    @classmethod
    def time(cls, line):
        if line == '':
            return None

        matchObj = cls.pattern.match(line)

        if matchObj is None:
            return None

        return datetime.datetime(
            int(matchObj.group("years")),
            int(matchObj.group("months")),
            int(matchObj.group("days")),
            int(matchObj.group("hours")),
            int(matchObj.group("minutes")),
            int(matchObj.group("seconds"))
            )

    @classmethod
    def data(cls, line):
        if line is None or line == '':
            return ["" for x in range(cls.dataCount)]
        matchObj = cls.pattern.match(line)
        return list(matchObj.groups()[cls.dataStart:])


class Ray(AnalyzeObject):

    pattern = re.compile("(\w) (?P<years>\d*) (?P<months>\d*) (?P<days>\d*) " +
                       "(?P<hours>\d*) (?P<minutes>\d*) (?P<seconds>\d*) " +
                       "(?P<microseconds>[0-9]*)\.[0-9]* " +
                       "(\d*) (\d*) (\d*) (\d*) (\d*) (\d*) " +
                       "([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*) ([0-9\-\.]*).*"
                       )

    dataStart = 8
    dataCount = 11


class Pressure(AnalyzeObject):

    pattern = re.compile(
        r'(?P<years>\d*)\s(?P<months>\d*)\s(?P<days>\d*)\s'
        r'(?P<hours>\d*)\s(?P<minutes>\d*)\s(?P<seconds>\d*)'
        r'\s([0-9\.\,]*)'
        )

    dataStart = 6
    dataCount = 1

    @classmethod
    def data(cls, line):
        data = AnalyzeObject.data.im_func(cls,line)
        return map(lambda d: d.replace(",", "."), data)


def merge(ostream, rline, pline):
    time = Ray.time(rline)
    data = Ray.data(rline) + Pressure.data(pline)
    time = time.timetuple()[:6]
    ostream.write(";".join(map(str, time)))
    ostream.write(";")
    ostream.write(";".join(data))
    ostream.write(";\n")


def run():
    if len(sys.argv) != 4:
        print "Usage: ./merge2.py raw_ray_file pressure_file outfile"
        sys.exit()

    # The file-opening part
    try:
        rays = io.BufferedReader(io.FileIO(sys.argv[1], "r"))
        pressures = io.BufferedReader(io.FileIO(sys.argv[2], "r"))
    except IOError:
        print "One of the files does not exist"
        try:
            rays.close()
            pressures.close()
        except:
            pass
        sys.exit()

    try:
        out = io.BufferedWriter(io.FileIO(sys.argv[3], "w"))
    except IOError:
        path = os.path.dirname(sys.argv[3])
        if not os.path.isdir(path):
            os.mkdir(path)

    try:
        out = io.BufferedWriter(io.FileIO(sys.argv[3], "w"))
    except:
        rays.close()
        pressures.close()
        raise

    # The real algorithm
    pline = pressures.readline()
    rline = rays.readline()
    ptime = None
    rtime = None

    while True:
        pline = pline.strip()
        rline = rline.strip()

        if rline == '' or pline == '':
            break  # we are done now

        if not rline.startswith("a"):
            rline = rays.readline()
            continue

        rtime = Ray.time(rline)
        if rtime is None:
            rline = rays.readline()
            continue

        if ptime is None:
            ptime = Pressure.time(pline)
            if ptime is None:
                pline = pressures.readline()
                continue
            pstart = ptime - datetime.timedelta(minutes=30)
            pend = ptime + datetime.timedelta(minutes=30)

        if rtime >= pstart and rtime <= pend:
            merge(out, rline, pline)
            rline = rays.readline()

        elif rtime < pstart:
            rline = rays.readline()

        else:
            pline = pressures.readline()
            ptime = None

    rays.close()
    pressures.close()
    out.close()


if __name__ == "__main__":
    run()
