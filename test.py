#!/usr/bin/env python
# coding: utf-8

import re
import io
from cStringIO import StringIO
import sys
from datetime import *

table = '''
x 0 0 0 0 0 0 0.0 0 0 0 0 0 0 0.0 0.0 0.0 0.0
a 2010 07 02 13 06 09 13878104.2 3688 4035 3689 243 225 248 1.0 1.0 1.0 33.0
a 2010 07 02 13 06 49 372548385.7 3660 3774 4095 62 87 0 1.0 1.0 1.0 33.0
a 2010 07 02 13 07 31 207010864.8 3736 4095 3576 1921 1863 1232 1.0 1.0 1.0 33.0
a 2010 07 02 13 08 51 753642902.1 3741 4095 3347 129 0 290 1.0 1.0 1.0 33.0
a 2010 07 02 13 09 23 741005635.2 3978 3777 3373 58 78 92 1.0 1.0 1.0 33.0
a 2010 07 02 13 10 32 309401676.3 3743 3707 4095 88 70 0 1.0 1.0 1.0 33.0
a 2010 07 02 13 10 39 697014271.6 4095 3771 3699 87 55 82 1.0 1.0 1.0 33.0
a 2010 07 02 13 11 10 269164739.0 3747 3720 4095 92 64 0 1.0 1.0 1.0 33.0
a 2010 07 02 13 13 06 568097210.7 3514 4095 3692 120 74 59 1.0 1.0 1.0 33.0
a 2010 07 02 13 14 19 715259438.7 2807 3781 4095 82 52 0 1.0 1.0 1.0 33.0
a 2010 07 02 13 15 24 409221581.5 4095 2620 4095 0 111 0 1.0 1.0 1.0 33.0
a 2010 07 02 13 16 05 907928580.6 3862 3781 3378 148 80 124 1.0 1.0 1.0 33.0
a 2010 07 02 13 16 48 677966201.7 3735 3643 4095 91 84 47 1.0 1.0 1.0 33.0
a 2010 07 02 13 17 27 543648657.6 3743 4095 2790 140 0 169 1.0 1.0 1.0 33.0
a 2010 07 02 13 18 16 276108336.3 3787 3599 3695 826 824 497 1.0 1.0 1.0 33.0
'''

def gen_table(self=None, treshold=timedelta(minutes=5)):
    origin = datetime(year=2000, month=1, day=1)
    pattern = re.compile("(\w) (?P<years>\d*) (?P<months>\d*) (?P<days>\d*) "+
                         "(?P<hours>\d*) (?P<minutes>\d*) (?P<seconds>\d*) "+
                         "(?P<microseconds>[0-9]*)\.[0-9]* "+
                         "(\d*) (\d*) (\d*) (\d*) (\d*) (\d*) "+
                         "([0-9\.]*) ([0-9\.]*) ([0-9\.]*) ([0-9\.]*).*"
                         )
    cummulated_data = [ 0 for x in range(10) ] + [1,]
    instream = StringIO(table)
    ostream = sys.stdout
    first = True
    for line in instream:
        mm = re.match(pattern, line)
        if mm is None: continue
        
        # preskocit v pripade jineho symbolu nez 'a'
        if mm.group(1) != 'a':
            continue
        
        cummulated_data[10] += 1
        actual = datetime(
            int(mm.group("years")),
            int(mm.group("months")),
            int(mm.group("days")),
            int(mm.group("hours")),
            int(mm.group("minutes")),
            int(mm.group("seconds")),
            int(mm.group("microseconds")) % 999999
            )
        delta = actual-origin
        if delta < treshold:
            #~ print "pricitam " + str(mm.groups()[1:8])
            for i in range(10):
                cummulated_data[i] += float(mm.group(i+9))
        else:
            # vytiskni do tabulky a dt2 = 0
            #~ print "Zapis vyvolan \n " + str(mm.groups()[1:8]) + "\n" + str(cummulated_data) + "\n"
            #~ print ""
            if not first:
                write_data(ostream, mm.groups()[1:8], cummulated_data)
            
            first = False
            # get ready for next run
            origin = actual
            cummulated_data = [ 0 for x in range(10) ] + [2,]

def write_data( ostream, dt, data):
    ostream.write("a")
    for d in dt: ostream.write(";"+str(d))
    for d in data: ostream.write(";"+str(d))
    ostream.write("\n")
    
gen_table()
